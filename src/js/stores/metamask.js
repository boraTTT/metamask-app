import { defineStore } from 'pinia'
import detectEthereumProvider from '@metamask/detect-provider'
import { ref } from 'vue'

export const useMetamask = defineStore('metamask', () => {
  const provider = detectEthereumProvider();
  const chainID = ref(null);
  const currentAccount = ref(null);
  const accounts = ref(null);

  const startApp = () => {
    if (provider !== window.ethereum) {
      console.log('Do you have multiple wallets installed?')
    }
  };

  const handleChainID = async () => {
    chainID.value = await window.ethereum.request({ method: 'eth_chainId' })
    window.ethereum.on('chainChanged', handleChainChanged);
    window.ethereum.on('accountsChanged', handleAccountsChanged);
  }

  const handleChainChanged = () => {
    window.location.reload();
  }

  const handleAccountsChanged = () => {
    if (accounts.value.length === 0) {
      console.log('Please connect to MetaMask.');
    }
    if (accounts.value[0] !== currentAccount.value) {
      currentAccount.value = accounts.value[0];
    }
  }

  const getAccount = async () => {
    try {
      accounts.value = await window.ethereum.request({ method: 'eth_requestAccounts' })
      currentAccount.value = accounts.value[0]
    } catch (err) {
      if (err.code === 4001) {
        console.log('Please connect Metamask.')
      } else {
        console.log('err: ', err)
      }
    }
  }

  return {
    provider,
    startApp,
    currentAccount,
    handleChainID,
    getAccount
  };
})
